<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';
include_once '../config/core.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

// set user property values

$user->password 	= $data->password;
$uuid 				= uniqid('', true);
$hash 				= $user->hashSSHA($user->password);
$encrypted_password = $hash["encrypted"]; // encrypted password
$salt 				= $hash["salt"]; // salt

$user->unique_id 			= $uuid;
$user->email 				= $data->email;
$user->encrypted_password 	= $encrypted_password;
$user->salt 				= $salt;
$user->role 				= $data->role;

$stmt = $user->countByEmail();
$num = $stmt->rowCount();

if($num>0){
	$user_arr = array(
		"message" 		=> 'Email telah terdaftar'
	);
	echo json_encode($user_arr);
}else{
	// register the user
	if($user->register()){
		
		$user_arr = array(
			"message" 		=> 'success',
			"email" 		=> $user->email,
			"role" 			=> $user->role
			);
		
		echo json_encode($user_arr);
	}else{
		$user_arr = array(
			"message" 		=> 'Registrasi gagal'
		);
		
		echo json_encode($user_arr);
	}
}

?>