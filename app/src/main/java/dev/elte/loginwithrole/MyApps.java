package dev.elte.loginwithrole;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

public class MyApps extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
    }

}
