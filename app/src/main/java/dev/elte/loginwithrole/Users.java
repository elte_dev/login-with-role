package dev.elte.loginwithrole;

import android.os.Parcel;
import android.os.Parcelable;

public class Users implements Parcelable {

    private String id;
    private String email;
    private String role;
    public final static Parcelable.Creator<Users> CREATOR = new Creator<Users>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Users createFromParcel(Parcel in) {
            return new Users(in);
        }

        public Users[] newArray(int size) {
            return (new Users[size]);
        }
    };

    protected Users(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.role = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Users() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(email);
        dest.writeValue(role);
    }

    public int describeContents() {
        return 0;
    }

}