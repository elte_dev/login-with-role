<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/user.php';
include_once '../config/core.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare user object
$user = new User($db);

$user->email = isset($_POST['email']) ? $_POST['email'] : die();
$user->password = isset($_POST['password']) ? $_POST['password'] : die();

$user->login();

// create array
$user_arr = array();
if($user->id===null){
	$user_arr = array(
		"message" 	=> 'Username atau password salah'
	);
}else{
	$user_arr = array(
		"message" 	=> 'success',
		"id" 		=> $user->id,
		"email" 	=> $user->email,
		"role" 		=> $user->role
	);
}

// make it json format
print_r(json_encode($user_arr));
	
?>