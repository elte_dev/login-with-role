package dev.elte.loginwithrole;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static dev.elte.loginwithrole.PrefUtil.clear;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();

    private TextView tvWelcome;
    private Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogout = findViewById(R.id.btnLogout);
        tvWelcome = findViewById(R.id.tvWelcome);

        Users users = PrefUtil.getUser(getApplicationContext(), PrefUtil.USER_SESSION);

        Log.e(TAG, "email "+ users.getEmail());
        Log.e(TAG, "role "+ users.getRole());

        if (users!=null)
            tvWelcome.setText(users.getEmail()+"\n"+users.getRole());

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "logout "+ "success");
                clear(getApplicationContext());
                finish();
            }
        });

    }
}
