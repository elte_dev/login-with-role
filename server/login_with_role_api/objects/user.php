<?php
class User{

	// database connection and table name
	private $conn;
	private $table_name = "users";

	// object properties
	public $id;
	public $unique_id;
	public $email;
	public $encrypted_password;
	public $password;
	public $salt;
	public $role;

	public function __construct($db){
		$this->conn = $db;
	}
	
	// register user
	function register(){
		// query to insert record			
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					unique_id=:unique_id, 
					email=:email,
					encrypted_password=:encrypted_password, 
					salt=:salt,
					role=:role
					";

		//sample data for insert
		//{"name":"John Doe","email":"a@a.com","password":"12345"}
		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->unique_id=htmlspecialchars(strip_tags($this->unique_id));
		$this->email=htmlspecialchars(strip_tags($this->email));
		$this->encrypted_password=htmlspecialchars(strip_tags($this->encrypted_password));
		$this->salt=htmlspecialchars(strip_tags($this->salt));
		$this->role=htmlspecialchars(strip_tags($this->role));

		// bind values
		$stmt->bindParam(":unique_id", $this->unique_id);
		$stmt->bindParam(":email", $this->email);
		$stmt->bindParam(":encrypted_password", $this->encrypted_password);
		$stmt->bindParam(":salt", $this->salt);
		$stmt->bindParam(":role", $this->role);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	//login user
	function login(){
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "
				WHERE
					email = ? 
				AND 
					encrypted_password = ? 
				LIMIT
					0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind id of user to be updated
		//$salt = "a7cd431b6e";
		//$salt =  salt;
        $password = $this->checkhashSSHA($this->getSalt($this->email),  $this->password);
		
		$stmt->bindParam(1, $this->email);
		$stmt->bindParam(2, $password);
		
		// execute query
		$stmt->execute();

		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);	
		
		// set values to object properties
		$this->id 		= $row['id'];
		$this->email 	= $row['email'];
		$this->role		 = $row['role'];
	}
	
	/**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
        return $hash;
    }
	
	public function getSalt($email) {
        $query = "SELECT
					salt
				FROM
					" . $this->table_name . "
				WHERE
					email = ?
				LIMIT
					0,1";
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->email);
		// execute query
		$stmt->execute();

		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);	
        return $row['salt'];
    }
	
	public function countByEmail(){
		$query = "SELECT 
					email 
				FROM 
					" . $this->table_name . "
				WHERE 
					email =? ";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->email);
		$stmt->execute();
		return $stmt;
	}
}
?>